package com.telerik;

import com.telerikacademy.MyArrayList;
import com.telerikacademy.MyList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MyArrayListTests {

    private MyList<Integer> list;

    @BeforeEach
    public void init() {
        list = new MyArrayList<>();
    }
    MyArrayList MyArrayList = new MyArrayList();



    @Test
    public void get_should_returnRightElement_when_indexIsValid() {
        list.add(1);
        list.add(2);
        list.add(3);

        Integer result = list.get(0);

        Assertions.assertEquals(1, result);
    }

    @Test
    public void get_should_throw_when_indexBelowZero() {

        Assertions.assertThrows(IndexOutOfBoundsException.class,
                () -> list.get(-1));


    }  @Test public void get_should_throw_when_indexIsAboveIndex() {
        Assertions.assertThrows(IndexOutOfBoundsException.class,
                ()-> list.get(0));

    }

    @Test public void getLast_should_returnRightElement_when_LastIndexIsValid() {
        list.add(1);
        list.add(2);
        list.add(3);

        Integer result = list.getLast();

        Assertions.assertEquals(3, result);
    } @Test public void getFirst_should_returnRightElement_when_FirstIndexIsValid() {
        list.add(1);
        list.add(2);
        list.add(3);

        Integer result = list.getFirst();

        Assertions.assertEquals(1,result);
    }@Test public void getSize_should_returnRight_whenIsValid() {
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);

        Integer size = list.getSize();

        Assertions.assertEquals(4,size);

    }@Test public void get_should_setValue_when_indexIsValid() {
        list.add(1);
        list.add(2);
        list.add(3);

        int number = 2;

        list.set(1,number);

        Assertions.assertEquals(list.get(1),number);

    } @Test public void array_should_addElements() {
        list = new MyArrayList<Integer>(new Integer[]{1,2,3});
        Assertions.assertEquals(1,list.get(0));
        Assertions.assertEquals(2,list.get(1));
        Assertions.assertEquals(3,list.get(2));



    } @Test public void get_should_resized_when_currentPositionIsEqualsToLast() {
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);

        Integer listSize = list.getSize();

        Assertions.assertEquals(8, listSize);

    }@Test public void findIndex_should_return_rightIndex_when_indexIsValid() {
        list.add(1);
        list.add(2);
        list.add(3);

        Integer index = list.findIndexOf(2);

        Assertions.assertEquals(1,index);


        //TODO it doesn't work
    } @Test public void hasNext_should_return_true_when_hasNextPosition() {
        list.add(1);
        list.add(2);
        list.add(3);

        Integer usedPositions = list.getUsedPositions();
        Integer notLastPosition = list.findIndexOf(2);
        boolean hasNext = notLastPosition < usedPositions;

        Assertions.assertTrue(hasNext);


        }

    }





