package com.telerik.UsersTest;

import com.telerikacademy.users.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;



    public class UserTests {

        @BeforeEach
                public void init() {

            User user = new User("       ");
        }



        @Test public void User_should_throw_when_usernameIsNull() {
            Assertions.assertThrows(NullPointerException.class,
                    ()-> new User(null));
        } @Test public void User_shouldThrow_when_usernameIsBellow1(){
            Assertions.assertThrows(IllegalArgumentException.class,
                    ()-> new User(""));
        }@Test public void User_shouldThrow_when_usernameIsAbove20() {
            Assertions.assertThrows(IllegalArgumentException.class,
                    ()-> new User("CoronaChaoChaoCoronaChaoChaoCoronaChaoChao"));

        } @Test public void getUsername_should_return_validName() {
                  User user = new User("Dani");
                  String name = user.getUsername();
                  Assertions.assertEquals(user.getUsername(),name);

        }


}
