from _lib import *

    
class SmokeTests(unittest.TestCase):
    
    def setUp(self):
        pass
    
    def tearDown(self):
        pass    
        
    def test_001_OpenForum(self):
        Forum.Start()
        
    def test_010_Test_CreateTopic(self):

        click (ForumUI.EnterSite)
        sleep (3)
        type ('https://stage-forum.telerikacademy.com/' + Key.ENTER)
        sleep (5)
	    click (ForumUI.Login)
	    sleep (3)
        click(ForumUI.NewTopic)
        sleep (1)
        type('Hello, WORLD')
        click(Pattern(ForumUI.Text).targetOffset(-85,23))
        sleep (1)
        type('The World is mine')
        click(ForumUI.CreateTopic)
        sleep (3) 
	
                

    def test_100_DeleteTopic(self):
        click(ForumUI.MyTopic)
        sleep (2)
        click(ForumUI.Points)
        sleep (1)
        click(ForumUI.Delete)
        sleep (1)

    def test_010_Test_Login(self):
        type('n', Key.CTRL + Key.SHIFT)
        wait(ForumUI.Incognito,5)
        type('https://stage-forum.telerikacademy.com/' + Key.ENTER)
        wait(ForumUI.Login2,5)
        click(ForumUI.Login2)
        sleep(5)
        click(ForumUI.Email)
        type('buffalo_war@abv.bg' + Key.TAB)
        type('testtest' + Key.ENTER)        
        sleep(4)
        assert exists(ForumUI.Profile)
        sleep(2)

    def test_010_Test_LogOut(self):
        click(ForumUI.Profile2)
        sleep(1)
        click(ForumUI.Buffalo)
        (sleep)
        click(ForumUI.LogOut)
        sleep(3)
        assert exists(ForumUI.LogIn2)


    def test_100_Close:
         Forum.Close()
         sleep(2)

     def test_100_Close:
         Forum.Close()

        

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(SmokeTests)
    
    print("WARNING: You may need to adapt UI map to match your windows specifics")
    outfile = open("Report.html", "w")
    runner = HTMLTestRunner.HTMLTestRunner(stream=outfile, title='SmokeTests Report' )
    runner.run(suite)
    outfile.close()

