########################################################
# UI map for XYZ
########################################################
from sikuli import *
########################################################

class ForumUI:
    EnterSite = "EnterSite.png"
    Login = "Login.png"
    NewTopic = "1599484206985.png"
    TitleTopic = "TitleField.png"
    Text = "1599483802329.png"
    CreateTopic = "createTopic.png"    
    MyTopic = "myTopic.png"
    Points = "1599485787272.png"
    Delete = "1599485817235.png"
    Incognito = "1600116674546.png"
    Login2 = "1600116874104.png"
    Email = "1600117117666.png"
    Profile = "1600117194739.png"
    Profile2 = "1600118131793.png"
    Buffalo = "1600118154817.png"
    LogOut = "1600118173985.png"
    LogIn2 = "1600118372219.png"
    