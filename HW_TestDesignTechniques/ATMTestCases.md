#Test Case 1

Withdrawl valid amount

Post-Condition:
500 BGN available balance

Test steps:
1-Insert the card
2-Select English language
3-Enter valid PIN and press OK
4-Select button "Withdraw"
5-Select 400 BGN
6-Select "Without SLIP"

Expected result:
funds were received.Available balance updated.

Priority – 1


#Test Case 2

Withdrawl valid amount

Post-Condition:
500 BGN available balance

Test steps:
1-Insert the card
2-Select English language
3-Enter valid PIN and press OK
4-Select button "Withdraw"
5-Select "Other amount"
6-Type 450
7-Click "OK"
8-Select "Without SLIP"

Expected result:
Funds were received.Available balance updated.

Priority – 1

#Test Case 3

Withdrawl invalid amount

Post-Condition:
500 BGN available balance

Test steps:
1-Insert the card
2-Select English language
3-Enter valid PIN and press OK
4-Select button "Withdraw"
5-Select "Other amount"
6-Type 600
7-Click "OK"
8-Select "Without SLIP"

Expected result:
Message "Insufficient funds".
Funds were not received.
Available balance was not updated.

Priority – 1

#Test Case 4

Enter invalid PIN

Post-Condition:
500 BGN available balance

Test steps:
1-Insert the card
2-Select English language
5-Enter invalid PIN and press OK



Expected result:
Message "Incorrect PIN"
Available balance was not updated.

Priority – 1

#Test Case 5

Withdrawl valid amount

Post-Condition:
500 BGN available balance

Test steps:
1-Insert the card
2-Select English language
3-Enter valid PIN and press OK
4-Select button "Withdraw"
5-Select "Other amount"
6-Type 499
7-Click "OK"
8-Select "Without SLIP"

Expected result:
Funds were received.Available balance updated.

Priority – 1

#Test Case 6

Withdrawl valid amount

Post-Condition:
500 BGN available balance

Test steps:
1-Insert the card
2-Select English language
3-Enter valid PIN and press OK
4-Select button "Withdraw"
5-Select "Other amount"
6-Type 500
7-Click "OK"
8-Select "Without SLIP"

Expected result:
Funds were received.Available balance updated.

Priority – 1

#Test Case 7

Withdrawl invalid amount

Post-Condition:
500 BGN available balance

Test steps:
1-Insert the card
2-Select English language
3-Enter valid PIN and press OK
4-Select button "Withdraw"
5-Select "Other amount"
6-Type 501
7-Click "OK"
8-Select "Without SLIP"

Expected result:
Message "Insufficient funds".
Funds were not received.
Available balance was not updated.

Priority – 1


