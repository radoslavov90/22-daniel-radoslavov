package JewelsInStones;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {

    public static int JewelsInStones(String J, String S){


        Map<Character,Integer> mapJ = getMap(J);
        Map<Character,Integer> mapS = getMap(S);


        int counts = 0;
        for (Character key :
                mapJ.keySet()) {
            counts += mapS.getOrDefault(key,0);

        }
        return counts;

    }private static Map<Character,Integer> getMap(String s) {
        Map<Character,Integer> map = new HashMap<>();
        for (char c : s.toCharArray()) {
            map.put(c, map.getOrDefault(c,0) + 1);
        }
        return map;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println(JewelsInStones(sc.nextLine(),sc.nextLine()));






    }
}
