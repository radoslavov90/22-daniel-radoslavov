package UniqueNumberOfoccurrences;

import java.util.Arrays;


class Solution {
    public static boolean uniqueOccurrences(int[] arr) {
        int len = arr.length;
        if (len < 2) {
            return true;
        }
        Arrays.sort(arr);
        int[] res = new int[len];
        int j = 0;
        int cnt = 0;

        for (int i = 0; i < len - 1; ++i) {
            if (arr[i] != arr[i + 1]) {
                res[cnt++] = ++j;
                j = 0;
            } else {
                j++;
            }
        }
        res[cnt] = j;
        if (arr[len - 2] == arr[len - 1]) {
            res[cnt++] = ++j;
        } else {
            res[cnt++] = 1;
        }
        for (int i = 0; i < cnt; ++i) {
            for (int k = 0; k < cnt; ++k) {
                if (res[i] == res[k] && k != i) {
                    return false;
                }
            }
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.println(uniqueOccurrences(new int[] {1,2,2,1,1,3}));
        System.out.println(uniqueOccurrences(new int[] {1,2}));
        System.out.println(uniqueOccurrences(new int[] {-3,0,1,-3,1,1,1,-3,10,0}));
    }
}