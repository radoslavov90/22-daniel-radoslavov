package Isomorphic;

import java.util.HashMap;

public class Isomorphic {
    public static boolean isIsomorphic(String s, String t) {
        if (s.length() != t.length())
            return false;
        HashMap<Character, Character> s1 = new HashMap<Character, Character>();
        HashMap<Character, Character> t2 = new HashMap<Character, Character>();
        for (int i = 0; i < s.length(); i ++){
            if (s1.containsKey(s.charAt(i))){
                if (s1.get(s.charAt(i)) != t.charAt(i)){
                    return false;
                }
            }
            if (t2.containsKey(t.charAt(i))){
                if (t2.get(t.charAt(i)) != s.charAt(i)){
                    return false;
                }
            }
            s1.put(s.charAt(i), t.charAt(i));
            t2.put(t.charAt(i), s.charAt(i));
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.println(isIsomorphic("egg","add"));
        System.out.println(isIsomorphic("foo","bar"));
        System.out.println(isIsomorphic("paper","title"));
    }
}