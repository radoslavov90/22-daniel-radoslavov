package testCases;


import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;


import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.junit.runners.Suite;
import pages.GooglePage;
import pages.TelerikStageForumPage;

public class TelerikStageForum extends BaseTest {
    GooglePage google = new GooglePage(actions.getDriver());
    TelerikStageForumPage forum = new TelerikStageForumPage(actions.getDriver());


    @Test
    public void userPath_when_creatingTopic() {

        forum.openUrl();
        forum.login();
        forum.waitToLoad(7000);
        actions.assertElementPresent("forum.NewTopicButton");
        forum.newTopic();
        forum.waitToLoad(5000);
        forum.openUrl();
        forum.isCreated();
        forum.logOut();
        forum.waitToLoad(5000);
        actions.assertElementPresent("forum.IsLogOut");

    }
}
