package testCases;

import com.telerikacademy.testframework.UserActions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import pages.GooglePage;

public class BaseTest {
	private static GooglePage google;
	UserActions actions = new UserActions();


	@BeforeClass
	public static void setUp(){
		UserActions.loadBrowser();


	}


	@AfterClass
	public static void tearDown(){
		UserActions.quitDriver();


	}
}
