package pages;

import com.telerikacademy.testframework.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TelerikStageForumPage extends BasePage{


    String telerikStageForum = Utils.getConfigPropertyByKey("forum.Url");

    public TelerikStageForumPage(WebDriver driver) {
        super(driver);
        super.setUrl(telerikStageForum);
    }
    public void openUrl() {
        Utils.getWebDriver().get(telerikStageForum);
    }



    public void login() {
        actions.clickElement("forum.LoginButton");
        actions.typeValueInField("buffalo_war@abv.bg","forum.EmailField");
        actions.typeValueInField("testtest","forum.PasswordField");
        actions.clickElement("forum.SendValuesButton");
    }

    public void newTopic() {
        actions.clickElement("forum.NewTopicButton");
        actions.typeValueInField("Test stageForum 38433" + actions.titleName(),"forum.Title");
        actions.typeValueInField("Check this song https://www.youtube.com/watch?v=sBlY53fgN-k","forum.Message");
        actions.clickElement("forum.CreateTopic");

    }

    public void logOut(){
        actions.clickElement("forum.Avatar");
        actions.clickElement("forum.LogOut");
        actions.clickElement("forum.LogOutConfirm");
    }


    public void isCreated() {
        actions.assertElementPresent("forum.TopicCreated");
    }



    public void waitToLoad(long milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }



}
