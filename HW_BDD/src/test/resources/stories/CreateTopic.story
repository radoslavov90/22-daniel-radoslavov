Meta:
@QA

Narrative:
As a user
I want to login
So that i can create topic

Scenario: Navigate Stage forum website
Given Click forum.LoginButton element
When Type buffalo_war@abv.bg in forum.EmailField field
When Type testtest in forum.PasswordField field
Then Click forum.SendValuesButton element
When Wait for 5000 milliseconds
Then Click forum.NewTopicButton element
When Type Hello in forum.Title field
When Type testtesttesttest in forum.Message field
Then Click forum.CreateTopic element