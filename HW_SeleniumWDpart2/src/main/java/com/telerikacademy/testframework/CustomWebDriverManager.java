package com.telerikacademy.testframework;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;


public class CustomWebDriverManager {
	public enum CustomWebDriverManagerEnum {
		INSTANCE;
		private WebDriver driver = setupBrowser();

		private WebDriver setupBrowser(){
			FirefoxOptions firefoxOptions = new FirefoxOptions();
			firefoxOptions.setProfile(new FirefoxProfile());
			firefoxOptions.addPreference("dom.webnotifications.enabled", false);
			firefoxOptions.addPreference("browser.cache.disk.enable", false);
			FirefoxDriverManager.getInstance().setup();
			WebDriver firefoxDriver = new FirefoxDriver(firefoxOptions);
			firefoxDriver.manage().window().maximize();
			driver = firefoxDriver;
			return firefoxDriver;

		}

		public void quitDriver() {
			if (driver != null) {
				driver.quit();
				driver = null;
			}
		}

		public WebDriver getDriver() {
			if (driver == null){
				setupBrowser();
			}
			return driver;
		}


	}
}
