package pages;

import com.telerikacademy.testframework.UserActions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {
    public WebDriver getDriver() {
        return driver;
    }

    protected void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    private WebDriver driver;

    private WebDriverWait wait;

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }

    private String Url;

    public UserActions actions;

    public BasePage(WebDriver driver) {
        this.driver = driver;
        actions = new UserActions();
    }
}
