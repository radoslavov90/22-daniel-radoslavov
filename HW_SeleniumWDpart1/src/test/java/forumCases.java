import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class forumCases {

    private static final String forumHomePage = "https://stage-forum.telerikacademy.com/";
    private WebDriver driver;
    private static final String title = "Samo Loko!";
    private static final String message = "Mani gi tia!Samo JAVA!!";


    @BeforeClass
    public static void ClassInit() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Dako\\Desktop\\drivers\\chromedriver.exe");
    }

    @Before
    public void TestInit() {
        driver = new ChromeDriver();
        driver.get(forumHomePage);
        LogIn(driver);

    }

    @After
    public void closeDriver() {
        driver.close();
    }

    @Test
    public void loginInForum() {
        boolean isItDisplayed = isDisplayed();
        Assert.assertTrue(isItDisplayed);
    }

    @Test
    public void createNewTopic_when_LogIn() {

        createNewTopic(title, message);
        driver.get(forumHomePage);
        boolean isItCreated = isCreated();
        Assert.assertTrue(isItCreated);

    }

    @Test
    public void deleteTopic_when_isCreated() {

        createNewTopic(title, message);
        waitToLoad();
        deleteTopic();
        boolean isItDeleted = isDeleted();
        Assert.assertTrue(isItDeleted);

    }

    @Test
    public void logOut_when_is_logIn() {
        logOut();
        waitToLoad();
        boolean isItLogOut = isLogOut();
        Assert.assertTrue(isItLogOut);


    }

    private boolean isLogOut() {
        return driver.findElement(By.xpath("//*[contains(text(),'New Topic')]")).isDisplayed();
    }

    private void logOut() {
        driver.findElement(By.className("avatar")).click();
        driver.findElement(By.xpath("//*[contains(text(),'buffalo_war')]")).click();
        driver.findElement(By.xpath("//*[contains(text(),'Log Out')]")).click();
    }

    private boolean isDeleted() {
        return driver.findElement(By.xpath("//*[contains(text(),'topic withdrawn by author, will be automatically deleted in 24 hours unless flagged')]")).isDisplayed();
    }

    private void waitToLoad() {
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    private void deleteTopic() {
        driver.findElement(By.xpath("//*[contains(@class,'ellipsis-h')]")).click();
        driver.findElement(By.xpath("//*[contains(@class,'trash')]")).click();
    }

    private void createNewTopic(String title, String message) {
        driver.findElement(By.xpath("//*[contains(text(),'New Topic')]")).click();
        driver.findElement(By.xpath("//*[contains(@class,'ember-text-field ember-view')]")).sendKeys(title);
        driver.findElement(By.xpath("//*[contains(@class,'d-editor-input')]")).sendKeys(message);
        driver.findElement(By.xpath("//*[contains(text(),'Create Topic')]")).click();
    }

    private void LogIn(WebDriver driver) {
        driver.findElement(By.className("d-button-label")).click();
        driver.findElement(By.id("Email")).sendKeys("buffalo_war@abv.bg");
        driver.findElement(By.id("Password")).sendKeys("testtest" + Keys.ENTER);
    }

    private boolean isDisplayed() {
        return driver.findElement(By.className("avatar")).isDisplayed();
    }

    private boolean isCreated() {
        return driver.findElement(By.xpath("//*[contains(text(),'Samo Loko!')]")).isDisplayed();
    }


}

