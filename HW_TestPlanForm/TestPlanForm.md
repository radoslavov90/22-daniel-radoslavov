# Test Plan


## Testing types
* Functional testing
* Usability testing
* Smoke testing
* Security Testing

##  Schedule
* Create the test specification - 90 man - hour
* Perform Test Execution - 40 man - hour
* Test Report  - 5 man - hour
* Test Delivery - 10 man - hour


## Scope of testing

### Functionalities to be tested
* Log in system
* New user registration*
* Change user settings
* Create a new topic
* Delete topic
* Edit topic

### Functionalities not to be tested
* Send private messages
* The search engine
* Functionality 3

## Exit Criteria
* All items in scoop was tested
* All priority 1 tests are passed and 80% of priority 2 tested are passed
* Time is up
